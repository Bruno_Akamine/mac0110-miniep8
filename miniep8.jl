function troca(v,i,j)
	aux=v[i]
	v[i]=v[j]
	v[j]=aux
end
function insercao(v)
	tam=length(v)
	for i in 2:tam
		j=i	
		while j>1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v,j,j-1)
			else
				break
			end
		j=j-1
		end
	end
	return v
end

function compareByValue(x,y)
	if x[1]=='A'
		intx=14
	elseif x[1]=='K'
		intx=13
	elseif x[1]=='Q'
		intx=12
	elseif x[1]=='J'
		intx=11
	else
		intx=parse(Int,x[1:length(x)-1])
	end
	if y[1]=='A'
		inty=14
	elseif y[1]=='K'
		inty=13
	elseif y[1]=='Q'
		inty=12
	elseif y[1]=='J'
		inty=11
	else
		inty=parse(Int,y[1:length(y)-1])
	end
	if intx<inty
		return true
	else
		return false
	end
end

function compareByValueAndSuit(x,y)
	if x[length(x)]=='♦'
		naipex=1
	elseif x[length(x)]=='♠'
		naipex=2
	elseif x[length(x)]=='♥'
		naipex=3
	else
		naipex=4
	end
	if y[length(y)]=='♦'
		naipey=1
	elseif y[length(y)]=='♠'
		naipey=2
	elseif y[length(y)]=='♥'
		naipey=3
	else
		naipey=4
	end
	if naipex<naipey
		return true
	elseif naipex>naipey
		return false
	else
		return compareByValue(x,y)
	end
end

using Test
function testes()
	@test compareByValue("2♠", "A♠")
	@test !compareByValue("K♥", "10♥")
	@test !compareByValue("10♠", "10♥")
	@test !compareByValue("10♥","3♠")
	@test compareByValue("Q♦","K♣")
	@test compareByValue("10♣","A♣")
	@test !compareByValue("A♠","A♣")
	@test compareByValueAndSuit("2♠", "A♠")
	@test !compareByValueAndSuit("K♥", "10♥")
	@test compareByValueAndSuit("10♠", "10♥")
	@test compareByValueAndSuit("A♠", "2♥")
	@test compareByValueAndSuit("3♦","10♦")
	@test compareByValueAndSuit("A♥","3♣")
	@test !compareByValueAndSuit("2♣","A♦")
	@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])==[ "10♦","J♠","K♠","A♠","A♠","10♥"]
	@test insercao(["7♣","10♥","2♦","A♦","Q♠","4♠","K♥","Q♣","3♣","K♣","J♦","10♦","9♥","A♥"])==["2♦","10♦","J♦","A♦","4♠","Q♠","9♥","10♥","K♥","A♥","3♣","7♣","Q♣","K♣"]
	@test insercao([])==[]
	@test insercao(["3♣"])==["3♣"]
	@test insercao(["3♣","3♠","3♥","3♦","3♣","3♥"])==["3♦","3♠","3♥","3♥","3♣","3♣"]
	println("Final dos testes")
end
testes()